
human(X) :- between(1,8,X).

% first two are infected
initially_infected(X) :- human(X), X<3.

% X\=Y  cant infect/meet yourself
0.1::contact(X,Y) :- human(X), human(Y), X\=Y.

0.1:: by_air.
0.6:: physical_contact.


0.25::resistant(X) :-human(X),\+initially_infected(X).

0.0::\+by_air :- resistant(X).
0.5::\+physical_contact :- resistant(X).

infected(X):-by_air, human(X).
infected(X):-initially_infected(X).
infected(X):-physical_contact, contact(X,Y), infected(Y).

evidence(resistant(3), true).
evidence(resistant(4), true).

query(infected(_)).


infected(1)	26:7
 1
infected(2)	26:7
 1
infected(3)	26:7
 0.12285252
infected(4)	26:7
 0.12285252
infected(5)	26:7
 0.12285252
infected(6)	26:7
 0.12285252
infected(7)	26:7
 0.12285252
infected(8)	26:7
 0.12285252
