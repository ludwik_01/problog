path(A,B) :-edge(A,B).
path(A,C) :-edge(A,B), B\=C, path(B,C).

0.5::edge(mall,gas).
0.6::edge(gas,car).
0.8::edge(car,highway).


0.6::edge(mall,radio).
0.4::edge(radio,roof).
0.2::edge(roof,heli).
0.99::edge(heli,fort).


query(path(mall,highway)).
query(path(mall,fort)).