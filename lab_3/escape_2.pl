path(A,B) :-edge(A,B).
path(A,C) :-edge(A,B), B\=C, path(B,C).

0.5::edge(mall,gas) :-go(highway),\+go(helicopter).
0.7::edge(gas,near_car) :-first(near_car),\+first(far_car).
0.4::edge(gas,far_car):-first(far_car),\+first(near_car).
0.5::edge(near_car,far_car).
0.3::edge(near_car,working_car).
0.7::edge(far_car,working_car).
0.8::edge(working_car,highway).


0.6::edge(mall,radio) :-go(helicopter),\+go(highway).
0.4::edge(radio,roof).
0.2::edge(roof,heli).
0.99::edge(heli,fort).


?::first(far_car).
?::first(near_car).
?::go(highway).
?::go(helicopter).


utility(path(mall,highway),50).
utility(path(mall,fort),100).
