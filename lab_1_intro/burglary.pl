neighbour(maxwell). neighbour(joan).

0.7::burglary.
0.01::earthquake(heavy); 0.19::earthquake(mild).

0.90::alarm_start :- burglary, earthquake(heavy).
0.85::alarm_start :- burglary, earthquake(mild).
0.80::alarm_start :- burglary, \+ earthquake(mild), \+earthquake(heavy).
0.30::alarm_start :- \+burglary, earthquake(heavy).
0.10::alarm_start :- \+burglary, earthquake(mild).

0.80::will_call(Name, alarm);
0.10::will_call(Name, prank) :- neighbour(Name).

% evidence(alarm_start, true).
% evidence(earthquake(mild), true).


neighbour_call(Neighbour) :- will_call(Neighbour, prank).
neighbour_call(Neighbour) :- will_call(Neighbour, alarm), alarm_start.

evidence(neighbour_call(maxwell), true).
evidence(neighbour_call(joan), false).

query(neighbour_call(_)).
query(burglary).
query(alarm_start).
query(earthquake(heavy)).
query(earthquake(mild)).

