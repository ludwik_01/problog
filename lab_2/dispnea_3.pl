
t(0.01)::visitedAsia.
t(0.30)::smoker.

t(_)::tubercolosis :- visitedAsia.
t(_)::tubercolosis :- \+visitedAsia.

t(0.3)::lung_cancer :- smoker.
t(_)::lung_cancer:- \+smoker.


t(0.2)::bronchitis :- smoker.
t(_)::bronchitis:- \+smoker.

t(_)::tb_or_lung_cancer:- tubercolosis,lung_cancer.
t(_)::tb_or_lung_cancer:- tubercolosis, \+lung_cancer.
t(_)::tb_or_lung_cancer:- \+tubercolosis, lung_cancer.
t(_)::tb_or_lung_cancer:- \+tubercolosis, \+lung_cancer.


t(_)::dyspnea:- tb_or_lung_cancer, bronchitis.
t(_)::dyspnea:- tb_or_lung_cancer, \+bronchitis.
t(_)::dyspnea:- \+tb_or_lung_cancer, bronchitis.
t(_)::dyspnea:- \+tb_or_lung_cancer, \+bronchitis.

t(0.6)::dyspnea.


t(_)::xray_positive:-tb_or_lung_cancer.
t(_)::xray_positive:- \+tb_or_lung_cancer.

query(dyspnea).

-23.30979653071025 [0.0, 0.62, 0.3593811, 0.02, 0.06451613, 0.0, 0.64663768, 0.17072931, 0.70979944, 0.0, 0.0, 1.0, 0.2280604, 0.00887717, 0.89609175, 0.65598342, 0.11701411, 0.0, 1.0] [t(0.01)::visitedAsia, t(0.3)::smoker, t(_)::tubercolosis, t(_)::tubercolosis, t(0.3)::lung_cancer, t(_)::lung_cancer, t(0.2)::bronchitis, t(_)::bronchitis, t(_)::tb_or_lung_cancer, t(_)::tb_or_lung_cancer, t(_)::tb_or_lung_cancer, t(_)::tb_or_lung_cancer, t(_)::dyspnea, t(_)::dyspnea, t(_)::dyspnea, t(_)::dyspnea, t(0.6)::dyspnea, t(_)::xray_positive, t(_)::xray_positive] 1122
