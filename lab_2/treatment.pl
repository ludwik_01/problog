
calcium.
bipho.

0.15::treat_by_calc :- calcium.
0.85::treat_by_bipho :- bipho.


0.5::\+treat_by_calc :-bipho.

osteoporosis_treatment:-treat_by_calc.
osteoporosis_treatment:-treat_by_bipho, \+calcium.

evidence(calcium,true).
evidence(bipho,true).

query(osteoporosis_treatment).
query(treat_by_bipho).
query(treat_by_calc).