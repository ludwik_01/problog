0.01::visit_asia.
0.5::smoker.

0.05::tuberculosis :- visit_asia.
0.01::tuberculosis :- \+visit_asia.

0.1::cancer :- smoker.
0.01::cancer:- \+smoker.

0.6::bronchitis :- smoker.
0.3::bronchitis:- \+smoker.

either :-tuberculosis.
either :-cancer.

0.9::dispnea:- either, bronchitis.
0.7::dispnea:- either, \+bronchitis.
0.8::dispnea:- \+either, bronchitis.
0.1::dispnea:- \+either, \+bronchitis.


0.98::positive_xray:-either.
0.05::positive_xray:- \+either.

query(dispnea).