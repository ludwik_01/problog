
t(_)::visitedAsia.
t(_)::smoker.
t(_)::tubercolosis :- visitedAsia.
t(_)::tubercolosis :- \+visitedAsia.
t(_)::lung_cancer :- smoker.
t(_)::lung_cancer:- \+smoker.
t(_)::bronchitis :- smoker.
t(_)::bronchitis:- \+smoker.

t(_)::tb_or_lung_cancer:- tubercolosis.
t(_)::tb_or_lung_cancer:- lung_cancer.



t(_)::dyspnea:- tb_or_lung_cancer, bronchitis.
t(_)::dyspnea:- tb_or_lung_cancer, \+bronchitis.
t(_)::dyspnea:- \+tb_or_lung_cancer, bronchitis.
t(_)::dyspnea:- \+tb_or_lung_cancer, \+bronchitis.


t(_)::xray_positive:-tb_or_lung_cancer.
t(_)::xray_positive:- \+tb_or_lung_cancer.

query(dyspnea).

-23.309796535589957 [0.0, 0.62, 0.00104744, 0.02, 0.06451613, 0.0, 0.86786643, 0.2686678, 1.0, 1.0, 0.9403125, 0.1306835, 0.27031079, 0.11659672, 1.0, 0.0] [t(_)::visitedAsia, t(_)::smoker, t(_)::tubercolosis, t(_)::tubercolosis, t(_)::lung_cancer, t(_)::lung_cancer, t(_)::bronchitis, t(_)::bronchitis, t(_)::tb_or_lung_cancer, t(_)::tb_or_lung_cancer, t(_)::dyspnea, t(_)::dyspnea, t(_)::dyspnea, t(_)::dyspnea, t(_)::xray_positive, t(_)::xray_positive] 845
